//Module dependencies
var express = require('express')

//Client route
module.exports = function(app) {
    app.use('/', express.static('dist'));
};