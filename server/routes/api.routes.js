'use strict';

var authenticate = require('../lib/authenticate'),
    vouchers = require('../controllers/vouchers.controller'),
    campaigns = require('../controllers/campaigns.controller');

//Api routes
module.exports = function(app) {
    //Api OK
    app.route('/api/ok').get((req, res) => {
        return res.json({api: "ok"});
    });

    //Vouchers routes
    let vouchersBasePath = '/api/vouchers';
    app.route(vouchersBasePath + '/:voucherId').get(vouchers.get);
    app.route(vouchersBasePath + '/use/:voucherId').get(authenticate, vouchers.use);

    //Campaign routes
    let campaignBasePath = '/api/campaigns';
    app.route(campaignBasePath + '/:campaignName').post(authenticate, campaigns.addVouchers);
    app.route(campaignBasePath).post(authenticate, campaigns.create);
};