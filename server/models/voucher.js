'use strict';

// Module dependencies
var mongoose = require('mongoose');

// Voucher schema
var VoucherSchema = new mongoose.Schema({
    value: {
        type: String,
        unique: true,
        required: true
    },
    campaign: {
        type: mongoose.Schema.ObjectId,
        ref: 'Campaign',
        required: true
    },
    discountType: {
        type: String,
        enum: ['rate', 'price'],
        required: 'discountType should be "rate", or "price".'
    },
    discountValue: {
        type: Number,
        required: 'discountValue can not be empty'
    },
    useLimit: {
        type: Number,
        default: 1
    }
});

VoucherSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

VoucherSchema.pre('validate', function (next) {
    if (this.useLimit <= 0) {
        next(new Error('Use limit must be greater than 0'));
    } else if (this.discountValue <= 0) {
        next(new Error('Discount value must be larger than 0'));
    } else if (this.discountType === 'rate') {
        if (this.discountValue > 100) {
            next(new Error('Discount value must be between 0 and 100 for rate discount'));
        } else {
            next();
        }
    } else {
        next();
    }
});

VoucherSchema.options.toJSON = VoucherSchema.options.toObject = {
    virtuals: false,
    transform: function (doc, ret, options) {
        //check if voucher is valid
        ret.isValid = doc.checkIfValid();

        delete ret._id;
        delete ret.__v;
        delete ret.campaign;
        delete ret.value;
        delete ret.useLimit;
    }
};

VoucherSchema.methods.checkIfValid = function checkIfValid() {
    if (this.campaign.type === 'periodic' && this.campaign.endDate < new Date()) {
        return false;
    } else {
        return this.useLimit > 0;
    }
};

// Export the Mongoose model
module.exports = mongoose.model('Voucher', VoucherSchema);