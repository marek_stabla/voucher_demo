'use strict';

// Module dependencies
var mongoose = require('mongoose');

// Client schema
var ClientSchema = new mongoose.Schema({
    apiKeyId: {
        type: String,
        unique: true,
        required: true
    },
    apiSecret: {
        type: String,
        required: true
    }
});

ClientSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Export the Mongoose model
module.exports = mongoose.model('Client', ClientSchema);