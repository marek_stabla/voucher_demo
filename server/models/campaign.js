'use strict';

// Module dependencies
var mongoose = require('mongoose');

// Campaign schema
var CampaignSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true,
        validate: {
            validator: function(v) {
                return /^[a-zA-Z0-9]+$/.test(v);
            },
            message: 'Name can contain only alphanumeric characters'
        }
    },
    client: {
        type: mongoose.Schema.ObjectId,
        ref: 'Client',
        required: true
    },
    type: {
        type: String,
        enum: ['endless', 'periodic'],
        required: 'type should be "endless", or "periodic".'
    },
    endDate: {
        type: Date
    },
    pushCount: {
        type: Number,
        default: 0
    }
});

CampaignSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

CampaignSchema.pre('validate', function(next) {
    if (this.type === 'periodic' && (!this.endDate || this.endDate < new Date())) {
        next(new Error('Valid date must be provided. Check number of daysToEnd.'));
    } else {
        next();
    }
});

CampaignSchema.methods.setDaysToEnd = function setDaysToEnd(days) {
    let endDate = new Date();
    endDate.setDate(new Date().getDate() + days);
    this.endDate = endDate;
};

// Export the Mongoose model
module.exports = mongoose.model('Campaign', CampaignSchema);