'use strict';

var Voucher = require('../models/voucher'),
    Client = require('../models/client');

exports.get = (req, res) => {

    let voucherId = req.params.voucherId.split('_')[1];
    let campaignName = req.params.voucherId.split('_')[0];

    Voucher.findOne({value: voucherId})
        .populate({
            path: 'campaign'
        })
        .exec((err, voucher) => {
            if (err) {
                return res.status(500).json({error: err});
            }

            if (!voucher || !voucher.campaign || voucher.campaign.name !== campaignName) {
                return res.status(404).send({message: 'Voucher not found'});
            } else {
                return res.status(200).json(voucher);
            }
        });
};

exports.use = (req, res) => {

    let voucherId = req.params.voucherId.split('_')[1];
    let campaignName = req.params.voucherId.split('_')[0];
    let client = req.client;

    Voucher.findOne({value: voucherId})
        .populate({
            path: 'campaign',
            populate: {
                path: 'client',
                select: '_id',
                model: Client
            }
        })
        .exec((err, voucher) => {
            if (err) {
                return res.status(500).json({error: err});
            }
            if (!voucher || !voucher.campaign || voucher.campaign.name !== campaignName || !voucher.campaign.client._id.equals(client._id)) {
                return res.status(404).send({message: 'Voucher not found'});
            } else {
                if (voucher.checkIfValid()) {
                    voucher.useLimit = voucher.useLimit - 1;
                    let response = (err) => {
                        if (err) {
                            return res.status(400).json({error: err});
                        }
                        return res.status(200).send({message: 'Voucher used succesfully'});
                    };

                    if (voucher.useLimit == 0) {
                        Voucher.remove({_id: voucher._id}, response);
                    } else {
                        Voucher.update({_id: voucher._id}, {
                            $set: {useLimit: voucher.useLimit}
                        }, response);
                    }
                } else {
                    return res.status(404).send({message: 'Voucher not found'});
                }
            }
        });
};
