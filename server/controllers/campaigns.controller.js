'use strict';

var Voucher = require('../models/voucher'),
    Campaign = require('../models/campaign'),
    shortid = require('shortid');

let createVouchers = (voucherInfo, campaign) =>
    new Promise((resolve, reject) => {

        if (voucherInfo.amount <= 0 || voucherInfo.amount > 100000) {
            return reject({status: 400, error: 'Amount must be a number between 1 and 100000'});
        }

        voucherInfo.campaign = campaign;
        voucherInfo.value = 'invalidateValue';
        //Create VoucherSchema based on request
        let voucher = new Voucher(voucherInfo);

        //Validate
        voucher.validate((err) => {
            if (err) {
                var errors = [];
                if (!err.errors) {
                    errors.push({message: err.message});
                } else if (err.name == 'ValidationError') {
                    for (let field in err.errors) {
                        errors.push({field: field, message: err.errors[field].message});
                    }
                }
                return reject({status: 400, error: errors});
            }

            //Generate vouchers with generated values
            let vouchers = [];
            let values = [];
            for (let i = 0; i < voucherInfo.amount; i++) {
                let voucherValue = voucherInfo.campaign.pushCount + shortid.generate();
                values.push(campaign.name + '_' + voucherValue);
                vouchers.push({
                    value: voucherValue,
                    campaign: voucherInfo.campaign._id,
                    discountType: voucherInfo.discountType,
                    discountValue: voucherInfo.discountValue,
                    useLimit: voucherInfo.useLimit
                });
            };

            //Inserting vouchers without validation
            Voucher.collection.insert(vouchers).then(() => {
                return resolve({status: 200, retVal: {message: 'Vouchers added to Your campaign.', vouchers: values}});
            }, (err) => {
                return resolve({status: 500, error: err});
            });
        });
    });

exports.create = (req, res) => {
    let campaign = new Campaign(req.body.campaign);
    campaign.client = req.client;

    if (campaign.type == 'periodic') {
        campaign.setDaysToEnd(req.body.campaign.daysToEnd);
    }

    Campaign.findOne({name: campaign.name}, (err, retCampaign) => {
        if (retCampaign) {
            return res.status(400).send({message: 'This campaign name is already taken.'});
        }

        campaign.validate((err) => {
            if (err) {
                var errors = [];
                if (!err.errors) {
                    errors.push({message: err.message});
                } else if (err.name == 'ValidationError') {
                    for (let field in err.errors) {
                        errors.push({field: field, message: err.errors[field].message});
                    }
                }
                return res.status(400).send({error: errors});
            }

            campaign.save((err, retCampaign) => {
                if (err) {
                    return res.status(500).send({error: err});
                }

                createVouchers(req.body.voucherInfo, retCampaign).then((success) => {
                    return res.status(success.status).send(success.retVal);
                }, (err) => {
                    return res.status(err.status).send({error: err.error});
                });

            });
        });

    });

};

exports.addVouchers = (req, res) => {
    let campaignName = req.params.campaignName;

    Campaign.findOne({name: campaignName, client: req.client.id}, (err, retCampaign) => {
        if (err) {
            return res.status(500).send({error: err});
        }

        if (!retCampaign) {
            return res.status(400).send({message: 'You do not own any campaign with that name.'});
        }

        retCampaign.pushCount++;

        Campaign.update({_id: retCampaign._id},
            {
                $set: {pushCount: retCampaign.pushCount}
            }, (err) => {
                if (err) {
                    return res.status(500).send({error: err});
                }

                createVouchers(req.body, retCampaign).then((success) => {
                    return res.status(success.status).send(success.retVal);
                }, (err) => {
                    return res.status(err.status).send({error: err.error});
                });
            });
        retCampaign.save()
    });
};