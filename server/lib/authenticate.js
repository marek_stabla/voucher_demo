'use strict';

//Module dependencies
var Buffer = require('buffer').Buffer;
var Client = require('../models/client.js');

//Basic auth authentication
module.exports = function (req, res, next) {

    if (!req.headers.authorization) {
        res.status(401).json({ error: 'No credentials sent!' });
    } else {
        let decoded = new Buffer(req.headers.authorization, 'base64').toString('utf8');
        let apiKeyId = decoded.split(':')[0];
        let apiSecret = decoded.split(':')[1];

        if (!apiKeyId || !apiSecret) {
            res.status(401).json({ error: 'Invalid credentials!' });
        } else {
            Client.findOne({ apiKeyId: apiKeyId, apiSecret: apiSecret}, (err, client) => {
                if (err || !client || client.length == 0) {
                    res.status(401).json({ error: 'Invalid credentials!' });
                } else {
                    req.client = client;
                    next();
                };
            });
        }
    }
};
