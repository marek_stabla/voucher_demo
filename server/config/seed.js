'use strict';

var Client = require('../models/client'),
    Campaign = require('../models/campaign'),
    Voucher = require('../models/voucher'),
    shortid = require('shortid');

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@$');

module.exports = function () {
    return new Promise((resolve, reject) => {
        console.log('Clearing document');

        //Seeding demo client
        let createClient = () =>
            new Promise((resolve, reject) => {
                Client.remove().exec().then(() => {
                    console.log("Clients documents cleared");
                    let client = new Client({apiKeyId: "demo", apiSecret: "demo"});
                    client.save((err, retClient) => {
                        if (err) {
                            console.error(err);
                            reject(err);
                        } else {
                            console.log("Client created", retClient);
                            resolve(retClient);
                        }
                    });
                });
            });

        let createCampaign = (client) =>
            new Promise((resolve, reject) => {
                Campaign.remove().exec().then(() => {
                    console.log("Campaigns documents cleared");
                    let campaign = new Campaign({name: "DEM", client: client, type: "periodic"});
                    campaign.setDaysToEnd(10);
                    campaign.save((err, retCampaign) => {
                        if (err) {
                            console.error(err);
                            reject(err);
                        } else {
                            console.log("Campaign created", retCampaign);
                            resolve(retCampaign);
                        }
                    });
                });
            });

        let createVouchers = (campaign) =>
            new Promise((resolve, reject) => {
                Voucher.remove().exec().then(() => {
                    console.log("Vouchers documents cleared");
                    let vouchers = [];
                    const numberOfVouchers = 5;
                    for (let i = 0; i < numberOfVouchers; i++) {

                        vouchers.push({
                            value: campaign.pushCount + shortid.generate(),
                            campaign: campaign._id,
                            discountType: 'rate',
                            discountValue: '50',
                            useLimit: 2
                        });
                    }

                    console.log(new Date(), "Inserting " + numberOfVouchers + " vouchers");
                    Campaign.update({_id: campaign._id}, {
                        $set: {pushCount: campaign.pushCount + 1}
                    }, (err) => {
                        if (err) {
                            return res.status(400).json({error: err});
                        }
                        Voucher.create(vouchers, () => {
                            console.log(new Date(), "Finished inserting vouchers");
                            resolve();
                        });
                    });
                });
            });

        createClient()
            .then(client => createCampaign(client))
            .then(campaign => createVouchers(campaign))
            .then(resolve);
    });
};