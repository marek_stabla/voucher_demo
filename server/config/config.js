'use strict';

module.exports = {
    mongoDbURI: process.env.MONGOLAB_URI,
    seedData: false
};