//Dependencies
var express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    apiRoutes = require('./server/routes/api.routes'),
    clientRoutes = require('./server/routes/client.routes'),
    config = require('./server/config/config'),
    seed = require('./server/config/seed'),
    cors = require('cors');

//initialize express
var app = express();


//connect to mongoose database
mongoose.connect(config.mongoDbURI, function (error) {
    if (error) console.error(error);
    else console.log('mongo connected');
});

//configure body-parser
app.use(bodyParser.json());

//enable All cors Requests
app.use(cors());

app.use(bodyParser.urlencoded({extended: true}));

//seed data
if (config.seedData) {
    seed().then(() => {
        console.log("Seed finished");
    });
}

apiRoutes(app);
clientRoutes(app);

module.exports = app;

