angular.module('app', [
        'ui.router',
        'app.pages'
    ])
    .constant("envConfig", {
        "baseApiUrl": "https://voucher-api-demo.herokuapp.com/api/"
    })
    .config(function myAppConfig($urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    })
    .run(function ($rootScope, $state, $stateParams, $http) {
        //;)
        $http.defaults.headers.common.Authorization = btoa('demo:demo');


        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.appName = "Voucher demo";
    })
    .controller('AppController', function AppCtrl($rootScope, $scope) {
        //App Controller
    });