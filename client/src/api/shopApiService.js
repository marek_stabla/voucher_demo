'use strict';

angular.module('app.api.shopApiService', [])
    .service('shopApiService', function ($q, $timeout) {

            this.getProducts = function () {
                var deferred = $q.defer();

                $timeout(function () {
                    var products = [];
                    for (var i = 1; i < 5; i++) {
                        products.push({
                            name: 'Przedmiot ' + i,
                            price: 10 * i
                        });
                    }

                    deferred.resolve({data: products});
                }, 100);
                return deferred.promise;
            }
        }
    );