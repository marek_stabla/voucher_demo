'use strict';

angular.module('app.api.vouchersApiService', [])
    .service('vouchersApiService', function ($http, envConfig) {
            var baseUrl = envConfig.baseApiUrl + 'vouchers';

            this.get = function (voucherId) {
                return $http({
                    method: 'GET',
                    url: baseUrl + '/' + voucherId
                });
            };

            this.use = function (voucherId) {
                return $http({
                    method: 'GET',
                    url: baseUrl + '/use/' + voucherId
                });
            };
        }
    );