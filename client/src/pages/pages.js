angular.module('app.pages', [
        'ui.router',
        'app.pages.shop',
        'app.pages.summary'
    ])
    .config(function config($stateProvider) {
        $stateProvider.state('pages', {
            abstract: true,
            controller: 'PagesController',
            templateUrl: 'pages/layout.tpl.html',
            resolve: {
                isAuthenticated: function ($rootScope) {
                    return $rootScope.isAuthenticated;
                }
            },
        });
    })
    .controller('PagesController',
        function PagesController($rootScope, $scope) {
            //Pages Controller
        });