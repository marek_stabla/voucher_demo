angular.module('app.pages.shop', [
        'app.api.shopApiService',
        'app.api.vouchersApiService'
    ])
    .config(function config($stateProvider) {
        $stateProvider.state('pages.shop', {
            url: '/',
            controller: 'ShopController',
            templateUrl: 'pages/shop/shop.tpl.html',
            resolve: {
                products: function (shopApiService) {
                    return shopApiService.getProducts();
                }
            },
            data: {
                pageTitle: 'Shop',
                pageClass: 'shop'
            }
        });
    })
    .controller('ShopController',
        function ShopController($scope, $timeout, products, vouchersApiService, $state) {
            $scope.products = products.data;
            $scope.voucher = {};

            $scope.cart = {sum: 0, sumAfterDiscount: 0};

            $scope.calculateCart = function () {
                var sum = 0;
                $scope.products.forEach(function(p) {
                    if (p.isChecked) {
                        sum += p.price;
                    }
                });

                $scope.cart.sum = sum;
                if ($scope.voucher.isValid) {
                    if ($scope.voucher.discountType == 'rate') {
                        $scope.cart.sumAfterDiscount = sum*(100-$scope.voucher.discountValue)/100;
                    } else {
                        $scope.cart.sumAfterDiscount = sum - $scope.voucher.discountValue;
                        if ($scope.cart.sumAfterDiscount < 0) {
                            $scope.cart.sumAfterDiscount = 0;
                        }
                    }
                }
            };

            $scope.onProductClick = function (product) {
                product.isChecked = !product.isChecked;
                $scope.calculateCart();
            };

            $scope.onVoucherChange = function () {
                $scope.voucher.status = 'checking';
                $scope.voucher.discountType = null;
                $scope.voucher.discountValue = null;
                $scope.voucher.isValid = false;
                $scope.calculateCart();

                if ($scope.voucher.value == '') {
                    $scope.voucherForm.voucher.$setPristine();
                    return;
                }

                vouchersApiService.get($scope.voucher.value).success(function (response) {
                    if (response.isValid) {
                        $scope.voucher.status = 'ok';
                        $scope.voucher.isValid = response.isValid;
                        $scope.voucher.discountType = response.discountType;
                        $scope.voucher.discountValue = response.discountValue;
                        $scope.calculateCart();
                    } else {
                        $scope.voucher.isValid = false;
                    }
                }).error(function (error) {
                    $scope.voucher.isValid = false;
                    $scope.voucher.status = 'failed';
                    $scope.voucher.errorMessage = error.message;
                    $scope.calculateCart();
                });
            };

            $scope.buy = function() {
                $scope.buying = true;
                $scope.bought = false;
                //Should perform call to shop api in order to buy product

                vouchersApiService.use($scope.voucher.value).success(function(response) {
                    $scope.bought = true;
                    $scope.usedVoucherMessage = response.message;
                    $timeout(function() {
                        $state.go('pages.summary');
                    }, 3000);
                }).error(function(err) {
                    console.log(err);
                    alert("Something went wrong. Please try again");
                });
            };
        });