angular.module('app.pages.summary', [

    ])
    .config(function config($stateProvider) {
        $stateProvider.state('pages.summary', {
            url: '/summary',
            controller: 'SummaryController',
            templateUrl: 'pages/summary/summary.tpl.html',
            resolve: {

            },
            data: {
                pageTitle: 'Summary',
                pageClass: 'summary'
            }
        });
    })
    .controller('SummaryController',
        function SummaryController($scope, $timeout) {

        });