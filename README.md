Voucher demo app based on Node.js & Angular
========================================
Local deployment
-----
1. Install Node.js, bower
2. Install dependencies
   ```
   npm install
   npm install -g bower
   npm install -g gulp
   bower install
   ```
3. Set Your mongodb address
   ```
   export MONGOLAB_URI="mongodb://..."
   ```
4. Build client app & start server
   ```
   gulp build
   node server.js
   ```
5. Start watch task if You want files to be refreshed for client side
   ```
   gulp watch
   ```

Heroku Install
-----
1. Follow the instructions from Heroku. Install the [Heroku Toolbelt](https://toolbelt.heroku.com/) and create node.js app with MongoDB.
2. On the command line, set your app's buildpack URL to target this repository:
   ```
   heroku config:set BUILDPACK_URL=https://github.com/robgraeber/heroku-buildpack-nodejs-bower-gulp.git
   ```
3. Allow the Heroku instance to install `devDependencies` stored in your `package.json` [as heroku recommends](https://devcenter.heroku.com/articles/nodejs-support#customizing-the-build-process).
   ```
   heroku config:set NPM_CONFIG_PRODUCTION=false
   ```
4. Add heroku to repository remotes and push
   ```
   git push heroku master
   ```