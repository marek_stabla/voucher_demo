'use strict';

const gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    mergeStream = require('merge-stream'),
    wiredep = require('wiredep')(),
    ngAnnotate = require('gulp-ng-annotate');

gulp.task('templates', () => {
    return gulp.src('client/src/**/*.html')
        .pipe(gulp.dest('dist/'));
});

gulp.task('styles-dep', () => {
    return gulp.src('client/src/dependencies.scss')
        .pipe(concat('dependencies.scss'))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist/styles'))
});

gulp.task('styles', () => {

    return gulp.src('client/src/app.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist/styles'));

});

gulp.task('scripts-dep', () => {
    return gulp.src(wiredep.js)
        .pipe(concat('dependencies.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('dist/scripts'));
});

gulp.task('scripts', () => {
    return gulp.src('client/src/**/*.js')
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(gulp.dest('dist/scripts'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify({mangle: false}))
        .pipe(gulp.dest('dist/scripts'));

});

gulp.task('assets', () => {
    return gulp.src('bower_components/bootstrap-sass/assets/fonts/bootstrap/glyphicons-halflings-regular.*')
        .pipe(gulp.dest('dist/fonts'));
});

gulp.task('build', ['templates', 'scripts-dep', 'scripts', 'styles-dep', 'styles', 'assets'], () => {
    console.log("Build completed");
});

gulp.task('watch', ['build'], () => {
    gulp.watch('client/src/**/*.js', ['scripts']);
    gulp.watch('client/src/**/*.scss', ['styles']);
    gulp.watch('client/src/**/*.html', ['templates']);
});

